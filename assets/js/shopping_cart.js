const $ = require('jquery');

$("input[name='product_quantity']").change(function (e) {
    console.log(e);
    let product_reference = e.currentTarget.dataset.reference;
    let product_quantity = e.target.attributes.value;
    console.log(product_quantity);
    // e.preventDefault();
    // $("form[name='submit_product_quantity_"+product_reference+"'");
    $.post({
        url: 'http://localhost:8000/mon-panier/',
        // type: 'POST',
        data: {
            'product_reference': product_reference,
            'product_quantity': product_quantity
        },
        success: function(response, status) {
            console.log('response', response);
            console.log('status', status);
        },
        error: function(result, status, error) {
            console.log('result', result);
            console.log('status', status);
            console.log('error', error);
        },
        complete: function (result, status) {
            console.log('result', result);
            console.log('status', status);
        }
    });
    console.log(product_reference);
});
