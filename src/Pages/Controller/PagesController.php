<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 20/02/19
 * Time: 18:47
 */

namespace App\Pages\Controller;


use App\Administration\Entity\Message;
use App\Administration\Form\MessageType;
use App\Shop\Entity\Patch;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PagesController
 * @package App\Pages\Controller
 */
class PagesController extends AbstractController
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @Route(path="/", name="home")
     * @return Response
     */
    public function home(): Response
    {
        /*$ref = preg_replace('/\./', '', microtime(true));
        $reference = "ORD" ."-".substr($ref, 8,strlen($ref)) . '-' . 1;
        dump($reference);
        $inv = preg_replace("/ORD/", "INV", $reference);
        dump($inv);*/
        return $this->render(
            'pages/home.html.twig'
        );
    }

    /**
     * @Route(path="/collections", name="collections")
     * @return Response
     */
    public function collections(): Response
    {
        return $this->render(
            'pages/collections.html.twig',
            [
                'patches' => $this->getDoctrine()->getRepository(Patch::class)->findAll()
            ]
        );
    }

    /**
     * @Route(path="/le-club", name="le_club")
     * @return Response
     */
    public function company(): Response
    {
        return $this->render(
            'pages/company.html.twig'
        );
    }

    /**
     * @Route(path="/contact", name="contact")
     * @param Request $request
     * @return Response
     */
    public function contact(Request $request): Response
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($message);
            die('message');
        }
        return $this->render(
            'pages/contact.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
