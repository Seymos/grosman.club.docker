<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 26/02/19
 * Time: 17:29
 */

namespace App\Shop\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class BaseOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $reference;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_creation;

    /**
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_ht;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_vat;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_ttc;

    public function __construct()
    {
        $this->date_creation = new \DateTime('now');
    }

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getTotalHt()
    {
        return $this->total_ht;
    }

    /**
     * @return mixed
     */
    public function getTotalVat()
    {
        return $this->total_vat;
    }

    /**
     * @return mixed
     */
    public function getTotalTtc()
    {
        return $this->total_ttc;
    }
}
