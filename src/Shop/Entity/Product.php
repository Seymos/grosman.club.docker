<?php

namespace App\Shop\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Shop\Repository\ProductRepository")
 */
class Product
{
    public const VAT = .2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_ht;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price_ttc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @var $shape
     * @ORM\ManyToOne(targetEntity="App\Shop\Entity\Shape", inversedBy="products", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(name="shape_id", referencedColumnName="id")
     */
    private $shape;

    /**
     * @var $color
     * @ORM\ManyToOne(targetEntity="App\Shop\Entity\Color", inversedBy="products", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     */
    private $color;

    /**
     * @var $patch
     * @ORM\ManyToOne(targetEntity="App\Shop\Entity\Patch", inversedBy="products", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(name="patch_id", referencedColumnName="id")
     */
    private $patch;

    /**
     * @var $size
     * @ORM\ManyToOne(targetEntity="App\Shop\Entity\Size", inversedBy="products", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(name="size_id", referencedColumnName="id")
     */
    private $size;

    /**
     * @var $quantity
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Shop\Entity\Ordering", inversedBy="products")
     * @ORM\JoinColumn(name="ordering_id", referencedColumnName="id")
     */
    private $ordering;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPriceHT(): ?float
    {
        return $this->price_ht;
    }

    public function setPriceHT(?float $price_ht): self
    {
        $this->price_ht = $price_ht;

        return $this;
    }

    public function getPriceTTC(): ?float
    {
        return $this->price_ttc;
    }

    public function setPriceTTC(?float $price_ttc): self
    {
        $this->price_ttc = $price_ttc;
        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(): void
    {
        $ref = preg_replace('/\./', '', microtime(true));
        $reference = "PROD-" . substr($ref, 5,strlen($ref));
        $this->reference = $reference;
    }

    public function getShape(): Shape
    {
        return $this->shape;
    }

    public function setShape(Shape $shape): self
    {
        $this->shape = $shape;
        return $this;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function setColor(Color $color): self
    {
        $this->color = $color;
        return $this;
    }

    public function getPatch(): Patch
    {
        return $this->patch;
    }

    public function setPatch(Patch $patch): self
    {
        $this->patch = $patch;
        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getVat(): float
    {
        return self::VAT;
    }

    /**
     * @return Size
     */
    public function getSize(): Size
    {
        return $this->size;
    }

    public function setSize(Size $size)
    {
        $this->size = $size;
        return $this;
    }

    public function getOrdering(): Ordering
    {
        return $this->ordering;
    }

    public function setOrdering(Ordering $ordering): self
    {
        $this->ordering = $ordering;
        return $this;
    }
}
