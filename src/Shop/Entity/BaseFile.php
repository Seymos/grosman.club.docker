<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 27/02/19
 * Time: 12:08
 */

namespace App\Shop\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

abstract class BaseFile
{
    protected const PUBLIC_PATH = __DIR__.'/../../../public/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var $name
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var $slug
     * @ORM\Column(type="string")
     */
    protected $slug;

    /**
     * @var $image
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(maxSize="20971520")
     */
    protected $image;

    /**
     * @var $path
     * @ORM\Column(type="string")
     */
    protected $path;

    /**
     * @var $pricing
     * @ORM\Column(type="float")
     */
    protected $pricing;

    /**
     * @var $quantity
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @var $is_fixture
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_fixture;

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setImage(UploadedFile $file = null): void
    {
        $this->image = $file->getFilename();
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     */
    public function setPricing($pricing): void
    {
        $this->pricing = $pricing;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return boolean
     */
    public function getIsFixture(): bool
    {
        return $this->is_fixture;
    }

    /**
     * @param boolean $is_fixture
     */
    public function setIsFixture($is_fixture): void
    {
        $this->is_fixture = $is_fixture;
    }

    /**
     * Returns absolute public path
     * @return string
     */
    public function getPublicPath(): string
    {
        return self::PUBLIC_PATH;
    }

    /**
     * Returns $entity fixtures path
     * Fixtures image folder : /public/$entityName/fixtures
     * @return string
     */
    public function getFixturesPath(): string
    {
        return self::PUBLIC_PATH.'uploads/'.$this->getLowerClassName().'/fixtures/';
    }

    /**
     * Returns /public/uploads/$entityName/
     * @return string
     */
    public function getUploadRootDir(): string
    {
        return self::PUBLIC_PATH . $this->getUploadDir();
    }

    /**
     * Image folder : /public/uploads/$entityName
     * @return string
     */
    public function getUploadDir(): string
    {
        return 'uploads/'.$this->getLowerClassName();
    }

    private function getLowerClassName(): string
    {
        $nameSpace = static::class;
        $rc = new \ReflectionClass($nameSpace);
        return strtolower($rc->getShortName());
    }
}
