<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 26/02/19
 * Time: 15:50
 */

namespace App\Shop\Entity;

use App\Administration\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Invoice
 * @ORM\Entity(repositoryClass="App\Shop\Repository\InvoiceRepository")
 * @ORM\HasLifecycleCallbacks()
 * @package App\Shop\Entity
 */
class Invoice extends BaseOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $paied;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_paied;

    /**
     * @ORM\OneToOne(targetEntity="App\Shop\Entity\Ordering", inversedBy="invoice")
     * @ORM\JoinColumn(name="ordering_id", referencedColumnName="id")
     */
    private $ordering;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $reference
     */
    public function setReference(string $reference): void
    {
        $this->reference = str_replace('/ORD/', 'INV', $reference);
    }

    /**
     * @return mixed
     */
    public function getPaied()
    {
        return $this->paied;
    }

    /**
     * @param boolean $paied
     */
    public function setPaied($paied): void
    {
        $this->paied = $paied;
    }

    /**
     * @return \DateTime
     */
    public function getDatePaied(): \DateTime
    {
        return $this->date_paied;
    }

    /**
     * @param \DateTime $date_paied
     */
    public function setDatePaied(\DateTime $date_paied): void
    {
        $this->date_paied = $date_paied;
    }

    /**
     * @return Ordering
     */
    public function getOrdering(): Ordering
    {
        return $this->ordering;
    }

    /**
     * @param Ordering $ordering
     */
    public function setOrdering($ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation): void
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @param mixed $total_ht
     */
    public function setTotalHt($total_ht): void
    {
        $this->total_ht = $total_ht;
    }

    /**
     * @param mixed $total_vat
     */
    public function setTotalVat($total_vat): void
    {
        $this->total_vat = $total_vat;
    }

    /**
     * @param mixed $total_ttc
     */
    public function setTotalTtc($total_ttc): void
    {
        $this->total_ttc = $total_ttc;
    }
}
