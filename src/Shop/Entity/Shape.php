<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 14:14
 */

namespace App\Shop\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Shop\Repository\ShapeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Shape extends BaseFile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var $product
     * @ORM\OneToMany(targetEntity="App\Shop\Entity\Product", mappedBy="shape")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function addProduct(Product $product): ?ArrayCollection
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            return $this->products;
        }
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

}
