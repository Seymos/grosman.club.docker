<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 26/02/19
 * Time: 11:25
 */

namespace App\Shop\Entity;

use App\Administration\Entity\User;
use App\Administration\Entity\UserAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Shop\Repository\OrderingRepository")
 * @ORM\HasLifecycleCallbacks()
 * @package App\Shop\Entity
 */
class Ordering extends BaseOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Shop\Entity\Product", mappedBy="ordering", cascade={"persist"}, fetch="EAGER")
     */
    protected $products;

    /**
     * @ORM\ManyToOne(targetEntity="App\Administration\Entity\User", inversedBy="orderings", cascade={"persist", "remove", "refresh"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var $shipping_address
     * @ORM\ManyToOne(targetEntity="App\Administration\Entity\UserAddress", inversedBy="orderings", cascade={"persist", "remove", "refresh"}, fetch="EAGER")
     * @ORM\JoinColumn(name="shipping_id", referencedColumnName="id")
     */
    protected $shipping_address;

    /**
     * @var $billing_address
     * @ORM\ManyToOne(targetEntity="App\Administration\Entity\UserAddress", inversedBy="orderings", cascade={"persist", "remove", "refresh"}, fetch="EAGER")
     * @ORM\JoinColumn(name="billing_id", referencedColumnName="id")
     */
    protected $billing_address;

    /**
     * @ORM\OneToOne(targetEntity="App\Shop\Entity\Invoice", mappedBy="ordering", cascade={"persist"})
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     */
    protected $invoice;

    public function __construct()
    {
        parent::__construct();
        $this->products = new ArrayCollection();
        $this->setReference();
        $this->total_ht = 0;
        $this->total_vat = 0;
        $this->total_ttc = 0;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\PreFlush()
     */
    protected function setReference(): void
    {
        $ref = preg_replace('/\./', '', microtime(true));
        $reference = "ORD-" . substr($ref, 8,strlen($ref));
        $this->reference = $reference;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): bool
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setOrdering($this);
            return true;
        } else {
            return false;
        }
    }

    public function removeProduct(Product $product): bool
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            $product->setOrdering(null);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param float $total_ht
     */
    public function setTotalHt(float $total_ht): void
    {
        $this->total_ht = $total_ht;
    }

    /**
     * @param float $total_vat
     */
    public function setTotalVat(float $total_vat): void
    {
        $this->total_vat = $total_vat;
    }

    /**
     * @param float $total_ttc
     */
    public function setTotalTtc(float $total_ttc): void
    {
        $this->total_ttc = $total_ttc;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    public function setInvoice(Invoice $invoice): self
    {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return UserAddress
     */
    public function getShippingAddress(): UserAddress
    {
        return $this->shipping_address;
    }

    /**
     * @param UserAddress $address
     */
    public function setShippingAddress(UserAddress $address): void
    {
        $this->shipping_address = $address;
    }

    /**
     * @return UserAddress
     */
    public function getBillingAddress(): UserAddress
    {
        return $this->billing_address;
    }

    /**
     * @param UserAddress $billing_address
     */
    public function setBillingAddress(UserAddress $billing_address): void
    {
        $this->billing_address = $billing_address;
    }


}
