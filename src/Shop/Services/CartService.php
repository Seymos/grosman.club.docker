<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 28/02/19
 * Time: 16:24
 */

namespace App\Shop\Services;

use App\Shop\Entity\Ordering;
use App\Shop\Entity\Product;
use App\Shop\Entity\Size;
use App\Shop\Repository\OrderingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{
    private $manager;
    private $repository;
    private $flashBag;

    public function __construct(EntityManagerInterface $manager, OrderingRepository $repository, FlashBagInterface $flashBag)
    {
        $this->manager = $manager;
        $this->repository = $repository;
        $this->flashBag = $flashBag;
    }

    /**
     * @param SessionInterface $session
     * @param Size $size
     * @param int $quantity
     * @return Ordering
     */
    public function validateAndCreateOrdering(SessionInterface $session, Size $size, int $quantity): Ordering
    {
        $product = $this->createProduct($session, $size, $quantity);
        $ordering = $this->addProductToOrder($session, $product);

        $session->remove('shape_id');
        $session->remove('color_id');
        $session->remove('patch_id');

        return $ordering;
    }

    /**
     * @param SessionInterface $session
     * @param Size $size
     * @param int $quantity
     * @return Product
     */
    public function createProduct(SessionInterface $session, Size $size, int $quantity): Product
    {
        $patch = $session->get('patch_id');
        $color = $session->get('color_id');
        $shape = $session->get('shape_id');
//        dump($shape->getPricing());
//        dump($color->getPricing());
//        dump($patch->getPricing());
//        dump($size->getPricing());
        $product = new Product();
        $product->setColor($color);
        $product->setShape($shape);
        $product->setPatch($patch);
        $product->setSize($size);
        $product->setQuantity($quantity);
        $total_ht = $color->getPricing() + $patch->getPricing() + $shape->getPricing() + $size->getPricing();
//        dump('total ht:',$total_ht);
        $product->setPriceHT($total_ht);
        $total_tva = Product::VAT * $total_ht;
//        dump('total tva:',$total_tva);
        $product->setPriceTTC($total_ht + $total_tva);
        $product->setReference();
//        dump($product);
        return $product;
    }

    /**
     * @param SessionInterface $session
     * @param Product $product
     * @return Ordering
     */
    public function addProductToOrder(SessionInterface $session, Product $product): Ordering
    {
        if (!$session->has('ordering')) {
            $ordering = new Ordering();
            $ordering->setStatus('waiting');
        } else {
            /** @var Ordering $ordering */
            $ordering = $session->get('ordering');
        }
        if (!in_array($product->getReference(), (array)$ordering->getProducts(), true)) {
            $ordering->addProduct($product);
            $ordering = $this->calculateOrderingTotalPricing($ordering);
        } else {
            dump($ordering);
            dump($product->getReference());
            exit('in array');
        }

        $session->set('ordering',$ordering);
        $session->set('ordering_reference', $ordering->getReference());
        return $ordering;
    }

    public function validateCart(Request $request): string
    {
        dump($request->request->get('submit'));
        switch ($request->request->get('submit')) {
            case 'validation':
                $this->flashBag->add('success', "Votre commande a été créée !");
                $route = 'cart_shipping';
                break;
            case 'save_and_purchase':
                $this->flashBag->add('success', "Votre commande vous attend !");
                $route = 'select_shape';
                break;
            case 'authenticate_and_validate':
                $this->flashBag->add('success', "Votre commande vous attend !");
                $route = 'cart_shipping';
                break;
            case 'purchase_not_connected':
                $this->flashBag->add('success', "Votre commande vous attend !");
                $route = 'select_shape';
                break;
            default:
                die('cart_validation_default_switch');
                break;
        }
        return $route;
    }

    private function calculateOrderingTotalPricing(Ordering $ordering): Ordering
    {
        /**
         * REVOIR TOUTE CETTE PARTIE, LE CALCUL DU HT SUR ORDERING EST FAUX
         */
        if (null === $ordering->getTotalHt()) {
            $total_ht = 0;
        } else {
            $total_ht = $ordering->getTotalHt();
        }
        /** @var Product $p */
        foreach ($ordering->getProducts() as $p) {
//            dump($p);
            $total_ht += $p->getPriceHT();
        }
//        dump($total_ht);
        $ordering->setTotalHt($total_ht);
        $ordering->setTotalVat(Product::VAT*$total_ht);
        $ordering->setTotalTtc($ordering->getTotalHt()+$ordering->getTotalVat());
        return $ordering;
    }
}
