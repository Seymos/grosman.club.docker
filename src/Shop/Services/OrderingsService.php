<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 28/02/19
 * Time: 15:06
 */

namespace App\Shop\Services;


use App\Administration\Entity\User;
use App\Shop\Entity\Ordering;
use App\Shop\Repository\OrderingRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderingsService
{
    private $manager;
    private $repository;

    public function __construct(EntityManagerInterface $manager, OrderingRepository $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    public function getLastOrdering(User $user): array
    {
        return $this->repository->getLastOrdering($user);
    }
}
