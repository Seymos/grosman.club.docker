<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 14:56
 */

namespace App\Shop\Repository;

use App\Shop\Entity\Shape;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
/**
 * @method Shape|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shape|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shape[]    findAll()
 * @method Shape[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShapeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Shape::class);
    }

    public function findAvailable()
    {
        return $this->createQueryBuilder('s')
            ->where('s.quantity > 0')
            ->andWhere('s.is_fixture = :is_fixture')
            ->setParameter('is_fixture', true)
            ->getQuery()
            ->getResult()
            ;
    }
}
