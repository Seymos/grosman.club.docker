<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 26/02/19
 * Time: 11:26
 */

namespace App\Shop\Repository;


use App\Administration\Entity\User;
use App\Shop\Entity\Ordering;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ordering|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ordering|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ordering[]    findAll()
 * @method Ordering[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ordering::class);
    }

    public function getLastOrdering(User $user)
    {
        return $this->createQueryBuilder('o')
            ->where('o.user = :user')
            ->setParameter('user',$user)
            ->getQuery()
            ->getResult()
            ;
    }
}