<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 23/02/19
 * Time: 18:44
 */

namespace App\Shop\Controller;


use App\Shop\Entity\Color;
use App\Shop\Entity\Patch;
use App\Shop\Entity\Shape;
use App\Shop\Entity\Size;
use App\Shop\Repository\ColorRepository;
use App\Shop\Repository\PatchRepository;
use App\Shop\Repository\ShapeRepository;
use App\Shop\Services\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FunnelController
 * @package App\Shop\Controller
 * @Route(path="/configuration")
 */
class FunnelController extends AbstractController
{
    private $shapeRepository;
    private $colorRepository;
    private $patchRepository;

    public function __construct(ShapeRepository $shapeRepository, ColorRepository $colorRepository, PatchRepository $patchRepository)
    {
        $this->shapeRepository = $shapeRepository;
        $this->colorRepository = $colorRepository;
        $this->patchRepository = $patchRepository;
    }

    /**
     * @Route(path="/forme", name="select_shape")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function shape(Request $request, SessionInterface $session): Response
    {
        if ($request->isMethod('POST') && null !== $request->request->get('shape_id')) {
            /** @var Shape $shape */
            $shape = $this->getDoctrine()->getRepository(Shape::class)->find($request->request->get('shape_id'));
            if ($shape instanceof Shape) {
                $session->set('shape_id', $shape);
                $this->addFlash('success',"Forme de casquette ok");
                return $this->redirectToRoute('select_color');
            } else {
                $this->addFlash('error',"Cette forme n'est plus disponible");
                return $this->redirectToRoute('select_shape');
            }
        }
        return $this->render(
            'shop/shape.html.twig',
            [
                'shapes' => $this->getDoctrine()->getRepository(Shape::class)->findAvailable(),
                'ordering' => $session->get('ordering') ?? $session->get('ordering') ?? null
            ]
        );
    }

    /**
     * @Route(path="/couleur", name="select_color")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function color(Request $request, SessionInterface $session): Response
    {
        if (!$session->has('shape_id')) {
            $this->addFlash('warning',"Veuillez d'abord sélectionner une forme");
            return $this->redirectToRoute('select_shape');
        }
        if ($request->isMethod('POST') && null !== $request->request->get('color_id')) {
            /** @var Color $color */
            $color = $this->getDoctrine()->getRepository(Color::class)->find($request->request->get('color_id'));
            if ($color instanceof Color) {
                $session->set('color_id', $color);
                $this->addFlash('success',"Couleur de casquette ok");
                return $this->redirectToRoute('select_patch');
            } else {
                $this->addFlash('error',"Cette couleur n'est plus disponible");
                return $this->redirectToRoute('select_color');
            }
        }
        return $this->render(
            'shop/color.html.twig',
            [
                'colors' => $this->getDoctrine()->getRepository(Color::class)->findAvailable()
            ]
        );
    }

    /**
     * @Route(path="/ecusson", name="select_patch")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function patch(Request $request, SessionInterface $session, CartService $cartService): Response
    {
        if (!$session->has('color_id')) {
            $this->addFlash('warning',"Veuillez d'abord sélectionner une couleur");
            return $this->redirectToRoute('select_color');
        }
        if ($request->isMethod('POST') && $request->request->get('patch_id') !== null) {
            /** @var Patch $patch */
            $patch = $this->getDoctrine()->getRepository(Patch::class)->find($request->request->get('patch_id'));
            if ($patch instanceof Patch) {
                $session->set('patch_id', $patch);
                $this->addFlash('success',"Patch de casquette ok");
                return $this->redirectToRoute('select_size');
            } else {
                $this->addFlash('success',"Cet écusson n'est plus disponible");
                return $this->redirectToRoute('select_patch');
            }
        }
        return $this->render(
            'shop/patch.html.twig',
            [
                'patches' => $this->getDoctrine()->getRepository(Patch::class)->findAvailable()
            ]
        );
    }

    /**
     * @Route(path="/taille", name="select_size")
     * @param Request $request
     * @return Response
     */
    public function sizeAndQuantity(Request $request, SessionInterface $session, CartService $cartService): Response
    {
        if (!$session->has('patch_id')) {
            $this->addFlash('warning',"Veuillez d'abord sélectionner un écusson personnalisé");
            return $this->redirectToRoute('select_patch');
        }
        $sizes = $this->getDoctrine()->getRepository(Size::class)->findAvailable();
        if ($request->isMethod('POST')) {
            $size = $this->getDoctrine()->getRepository(Size::class)->find($request->request->get('product_size'));
            $quantity = $request->request->get('quantity');
            $ordering = $cartService->validateAndCreateOrdering($session, $size, $quantity);
            return $this->redirectToRoute('cart', ['reference' => $ordering->getReference()]);
        }
        return $this->render(
            'shop/size_and_quantity.html.twig',
            [
                'sizes' => $sizes
            ]
        );
    }

    public function cartRender()
    {
        return $this->render(
            'shop/cart_render.html.twig'
        );
    }
}
