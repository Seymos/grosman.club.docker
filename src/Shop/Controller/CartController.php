<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 27/02/19
 * Time: 23:18
 */

namespace App\Shop\Controller;


use App\Account\Form\RegisterType;
use App\Account\Form\ShippingType;
use App\Administration\Entity\User;
use App\Administration\Entity\UserAddress;
use App\Shop\Entity\Ordering;
use App\Shop\Entity\Product;
use App\Shop\Services\CartService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class CartController
 * @package App\Shop\Controller
 * @Route(path="/mon-panier")
 */
class CartController extends AbstractController
{
    /**
     * @Route(path="/commande/{reference}", name="cart")
     * @param Request $request
     * @param SessionInterface $session
     * @param CartService $cartService
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function cart(Request $request, SessionInterface $session, CartService $cartService, EntityManagerInterface $manager, string $reference = null): Response
    {
        if (!$session->has('ordering') && null === $reference) {
            return $this->redirectToRoute('select_size');
        }

        if (($ordering = $session->get('ordering')) instanceof Ordering) {
            /** @var Ordering $ordering */
            $ordering = $session->get('ordering');
        } else {
            $ordering = $this->getDoctrine()->getRepository(Ordering::class)->findOneBy(['reference' => $reference]);
            $session->set('ordering', $ordering);
        }

        if  ($this->getUser()) {
            /** @var User $current_user */
            $current_user = $this->getUser();
            $ordering->setUser($current_user);
        } else {
            $current_user = null;
        }
        if ($request->isMethod('POST') && 'cart_validation' === $request->request->get('submit')) {
            $total_ht = $request->request->get('ordering_total_ht');
            dump($total_ht);
            $ordering->setTotalHt($total_ht);
            $ordering->setTotalVat(Product::VAT * $total_ht);
            $ordering->setTotalTtc($ordering->getTotalHt() + $ordering->getTotalVat());
            $manager->persist($ordering);
            $manager->flush();
            $this->addFlash('success',"Votre commande a été enregistrée");
            return $this->redirectToRoute('cart_shipping');
        }

        return $this->render(
            'shop/cart.html.twig',
            [
                'ordering' => $session->get('ordering')
            ]
        );
    }

//    /**
//     * @Route(path="/quantity/{reference}", name="change_product_quantity")
//     * @param string $reference
//     * @param Request $request
//     * @param SessionInterface $session
//     * @return Response
//     */
//    public function changeQuantity(string $reference, Request $request, SessionInterface $session): Response
//    {
//        dump($reference);
//        $regex = '/^submit_product_quantity_[A-Z0-9-]+$/';
//        if ($request->isMethod('POST')) {
//            die('here');
//        }
//        if ($request->isMethod('POST') && preg_match($regex, $request->request->get('submit'))){
//            /** @var Ordering $ordering */
//            $ordering = $session->get('ordering');
//            /** @var Product $product */
//            foreach ($ordering->getProducts() as $product) {
//                if ($request->request->get('product_reference') === $product->getReference()) {
//                    $clone_product = clone $product;
//                    dump($product);
//                    dump($clone_product);
//                    $product->setQuantity($request->request->get('product_qte'));
//                }
//            }
//            $session->set('ordering', $ordering);
//            $this->addFlash('success', 'Quantité changée avec succès !');
//            return $this->redirectToRoute('cart_validation');
//        } else {
//            return $this->redirectToRoute('cart_validation');
//        }
//    }


    /**
     * @Route(path="/join", name="cart_join")
     * @return Response
     */
    public function join(Request $request, AuthenticationUtils $authenticationUtils, SessionInterface $session, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        $last_username = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));
            $em = $this->getDoctrine()->getManager();
            $this->addFlash('success',"Votre compte a été créé !");
            $token = new UsernamePasswordToken($user, $user->getPlainPassword(), 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            if ($session->has('ordering_reference')) {
                /** @var Ordering $ordering */
                $ordering = $this->getDoctrine()->getRepository(Ordering::class)->findOneBy(['reference' => $session->get('ordering_reference')]);
                $user->addOrdering($ordering);
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('cart_shipping');
            }
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('account_profile');
        }
        return $this->render(
            'shop/join.html.twig',
            [
                'form' => $form->createView(),
                'last_username' => $last_username,
                'error' => $error
            ]
        );
    }

    /**
     * @Route(path="/livraison", name="cart_shipping")
     * @Security("is_granted('ROLE_USER')")
     * @return Response
     */
    public function shipping(Request $request, SessionInterface $session, EntityManagerInterface $manager): Response
    {
        $shipping = new UserAddress();
        /** @var User $user */
        $user = $this->getUser();
        $shipping->setType('shipping');
        $form = $this->createForm(ShippingType::class, $shipping);
        /** @var Ordering $ordering */
        $ordering = $this->getDoctrine()->getRepository(Ordering::class)->findOneBy(['reference' => $session->get('ordering')->getReference()]);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && null !== $request->request->get('shipping_address')) {
            $shipping = $this->getDoctrine()->getRepository(UserAddress::class)->find((int)$request->request->get('shipping_address'));
            $ordering->setShippingAddress($shipping);
            if ($request->request->get('billing') === null) {
                $manager->persist($ordering);
                $manager->flush();
                $session->set('ordering', $ordering);
                $this->addFlash('success',"Adresse de livraison enregistrée !");
                return $this->redirectToRoute('cart_billing');
            } else {
                $ordering->setBillingAddress($shipping);
                $manager->persist($ordering);
                $manager->persist($user);
                $manager->flush();
                $session->set('ordering', $ordering);
                $this->addFlash('success',"Adresse de livraison et de facturation enregistrées !");
                return $this->redirectToRoute('cart_pay', ['reference' => $ordering->getReference()]);
            }
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $ordering->setShippingAddress($shipping);
            $user->addAddress($shipping);
            $manager->persist($shipping);
            if ($request->request->get('billing') === null) {
                $manager->persist($ordering);
                $manager->persist($user);
                $manager->flush();
                $this->addFlash('info', "Adresse de livraison enregistrée !");
                return $this->redirectToRoute('cart_billing');
            } else {
                $billing = clone $shipping;
                $billing->setType('billing');
                $user->addAddress($billing);
                $ordering->setBillingAddress($billing);
                $manager->persist($billing);
                $manager->persist($ordering);
                $manager->persist($user);
                $manager->flush();
                $this->addFlash('info', "Adresses de livraison et de facturation enregistrées !");
                return $this->redirectToRoute('cart_pay', ['reference' => $ordering->getReference()]);
            }
        }
        return $this->render(
            'shop/shipping.html.twig',
            [
                'form' => $form->createView(),
                'addresses' => $this->getUser()->getAddresses(),
                'ordering' => $ordering
            ]
        );
    }

    /**
     * @Route(path="/facturation", name="cart_billing")
     * @param Request $request
     * @return Response
     */
    public function billing(Request $request, SessionInterface $session, EntityManagerInterface $manager): Response
    {
        $newBilling = new UserAddress();
        $newBilling->setType('billing');
        $form = $this->createForm(ShippingType::class, $newBilling);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $request->request->get('billing_address') !== null) {
            $billing = $this->getDoctrine()->getRepository(UserAddress::class)->find((int)$request->request->get('billing_address'));
            /** @var Ordering $ordering */
            $ordering = $session->get('ordering');
            $ordering->setShippingAddress($billing);
            $session->set('ordering', $ordering);
            return $this->redirectToRoute('cart_pay');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();
            $user->addAddress($newBilling);
            /** @var Ordering $ordering */
            $ordering = $session->get('ordering');
            $ordering->setBillingAddress($newBilling);
            $session->set('ordering', $ordering);
            $manager->persist($newBilling);
            $manager->persist($user);
            $manager->persist($ordering);
            $manager->flush();
            $this->addFlash('success',"Adresse de facturation enregistrée !");
            return $this->redirectToRoute('cart_pay', ['reference' => $ordering->getReference()]);
        }
        return $this->render(
            'shop/billing.html.twig',
            [
                'form' => $form->createView(),
                'addresses' => $this->getUser()->getAddresses(),
                'ordering' => $session->get('ordering')
            ]
        );
    }

    /**
     * @Route(path="/paiement/{reference}", name="cart_pay")
     * @return Response
     */
    public function pay(Request $request, SessionInterface $session, string $reference): Response
    {
        if ($reference !== $session->get('ordering')->getReference()) {
            $this->addFlash('error',"Cette commande n'existe pas");
            return $this->redirectToRoute('account_orderings');
        }
        if ($request->isMethod('POST') && (null !== $request->request->get('submit'))) {
            switch ($request->request->get('checkout_type')) {
                case 'stripe':
                    exit('stripe checkout');
                    break;
                case 'paypal':
                    exit('paypal checkout');
                    break;
                case 'credit_card':
                    exit('credit card checkout');
                    break;
                default:
                    exit('defautl checkout');
            }
        }
        return $this->render(
            'shop/pay.html.twig',
            [
                'ordering' => $this->getDoctrine()->getRepository(Ordering::class)->findOneBy(['reference' => $reference])
            ]
        );
    }

    /**
     * @Route(path="/delete/{reference}", name="cart_remove_product")
     * @return Response
     */
    public function removeProduct(string $reference, SessionInterface $session): Response
    {
        /** @var Ordering $ordering */
        $ordering = $session->get('ordering');
        $products = $ordering->getProducts();
        /**
         * @var int $k
         * @var Product $product
         */
        foreach ($products as $k => $product) {
            if ($product->getReference() === $reference) {
                $products->removeElement($product);
                $message = "Produit supprimé du panier";
            }
        }
        $this->addFlash('info',$message);
        return $this->redirectToRoute('cart_validation');
    }

    /**
     * @Route(path="/vider", name="cart_clear")
     * @param SessionInterface $session
     * @return Response
     */
    public function emptyCart(SessionInterface $session): Response
    {
        $referer = $_SERVER['HTTP_REFERER'];
        if ($session->has('ordering')) {
            $session->remove('ordering');
        }
        return $this->redirect($referer);
    }
}
