<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 20/02/19
 * Time: 18:56
 */

namespace App\Administration\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @Route(path="/admin", name="admin")
     * @return Response
     */
    public function admin()
    {
        return $this->render('administration/dashboard.html.twig');
    }
}