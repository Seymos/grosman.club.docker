<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 26/02/19
 * Time: 11:08
 */

namespace App\Administration\Controller;


use App\Administration\Form\PatchType;
use App\Shop\Entity\Patch;
use App\Shop\Repository\PatchRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PatchController
 * @package App\Administration\Controller
 * @Route(path="/admin/patches")
 */
class PatchController extends AbstractController
{
    private $repository;

    public function __construct(PatchRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(path="/", name="patch_list")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function list(Request $request, EntityManagerInterface $manager): Response
    {
        $patch = new Patch();
        $form = $this->createForm(PatchType::class, $patch);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $patch->setSlug($slugger->slugify($patch->getName()));
            $manager->persist($patch);
            $manager->flush();
            $this->addFlash('success',"Nouvel écusson ajouté !");
            return $this->redirectToRoute('patch_list');
        }
        return $this->render(
            'administration/patches/list.html.twig',
            [
                'patches' => $this->repository->findAll(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/edit", name="patch_edit")
     * @ParamConverter("patch", class="App\Shop\Entity\Patch")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $manager, Patch $patch): Response
    {
        $form = $this->createForm(PatchType::class, $patch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $patch->setSlug($slugger->slugify($patch->getName()));
            $manager->persist($patch);
            $manager->flush();
            $this->addFlash('success',"Ecusson modifié !");
            return $this->redirectToRoute('patch_list');
        }
        return $this->render(
            'administration/patches/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/remove", name="patch_remove")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Patch $patch
     * @return Response
     */
    public function remove(Request $request, EntityManagerInterface $manager, Patch $patch): Response
    {
        if ($patch) {
            $manager->remove($patch);
            $manager->flush();
            $this->addFlash('success',"Ecusson supprimé avec succès !");
            return $this->redirectToRoute('patch_list');
        } else {
            $this->addFlash('success',"Ecusson non trouvée !");
            return $this->redirectToRoute('patch_list');
        }
    }
}
