<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 17:37
 */

namespace App\Administration\Controller;


use App\Administration\Form\SizeType;
use App\Shop\Entity\Size;
use App\Shop\Repository\SizeRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SizeController
 * @package App\Administration\Controller
 * @Route(path="/admin/sizes")
 */
class SizeController extends AbstractController
{
    private $repository;

    public function __construct(SizeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(path="/", name="size_list")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function list(Request $request, EntityManagerInterface $manager): Response
    {
        $size = new Size();
        $form = $this->createForm(SizeType::class, $size);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $size->setSlug($slugger->slugify($size->getName()));
            $manager->persist($size);
            $manager->flush();
            $this->addFlash('success',"Nouvelle taille ajoutée !");
            return $this->redirectToRoute('size_list');
        }
        return $this->render(
            'administration/sizes/list.html.twig',
            [
                'sizes' => $this->repository->findAll(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/edit", name="size_edit")
     * @ParamConverter("size", class="App\Shop\Entity\Size")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $manager, Size $size): Response
    {
        $form = $this->createForm(SizeType::class, $size);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $size->setSlug($slugger->slugify($size->getName()));
            $manager->persist($size);
            $manager->flush();
            $this->addFlash('success',"Taille modifiée !");
            return $this->redirectToRoute('size_list');
        }
        return $this->render(
            'administration/sizes/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/remove", name="size_remove")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Size $size
     * @return Response
     */
    public function remove(Request $request, EntityManagerInterface $manager, Size $size): Response
    {
        if ($size) {
            $manager->remove($size);
            $manager->flush();
            $this->addFlash('success',"Taille supprimée avec succès !");
            return $this->redirectToRoute('size_list');
        } else {
            $this->addFlash('success',"Taille non trouvée !");
            return $this->redirectToRoute('size_list');
        }
    }
}
