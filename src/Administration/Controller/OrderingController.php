<?php


namespace App\Administration\Controller;


use App\Shop\Entity\Ordering;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderingController
 * @package App\Administration\Controller
 * @Route(path="commandes")
 */
class OrderingController extends AbstractController
{
    /**
     * @Route(path="/", name="last_orderings")
     */
    public function lastOrderings()
    {
        $repository = $this->getDoctrine()->getRepository(Ordering::class);

        return $this->render('administration/orderings/last.html.twig', ['last_orderings' => $repository->findAll()]);
    }

    public function pendingOrderings()
    {}

    public function allOrderings()
    {}

    public function ordering()
    {}
}
