<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 17:56
 */

namespace App\Administration\Controller;


use App\Administration\Form\ShapeType;
use App\Administration\Services\FileManager;
use App\Shop\Entity\Shape;
use App\Shop\Repository\ShapeRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShapeController
 * @package App\Administration\Controller
 * @Route(path="/admin/shapes")
 */
class ShapeController extends AbstractController
{
    private $repository;
    
    public function __construct(ShapeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(path="/", name="shape_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request, EntityManagerInterface $manager, FileManager $fileManager): Response
    {
        $shape = new Shape();
        $form = $this->createForm(ShapeType::class, $shape);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($shape);
            die('add shape');
            $slugger = new Slugify();
            $shape->setSlug($slugger->slugify($shape->getName()));
            $fileManager->upload($shape->getImage(), $this->getParameter('shape_dir'), $shape->getSlug());
            //$shape->setImage($filename);
            $shape->setPath($this->getParameter('shape_dir'));
            $manager->persist($shape);
            $manager->flush();
            $this->addFlash('success',"Forme ajoutée !");
            return $this->redirectToRoute('shape_list');
        }
        return $this->render(
            'administration/shapes/list.html.twig',
            [
                'shapes' => $this->repository->findAll(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/edit", name="shape_edit")
     * @ParamConverter("shape", class="App\Shop\Entity\Shape")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $manager, Shape $shape): Response
    {
        $form = $this->createForm(ShapeType::class, $shape);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $shape->setSlug($slugger->slugify($shape->getName()));
            $manager->persist($shape);
            $manager->flush();
            $this->addFlash('success',"Forme modifiée !");
            return $this->redirectToRoute('shape_list');
        }
        return $this->render(
            'administration/shapes/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/remove", name="shape_remove")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Shape $shape
     * @return Response
     */
    public function remove(Request $request, EntityManagerInterface $manager, Shape $shape, FileManager $fileManager): Response
    {
        if ($shape) {
            if ($fileManager->remove($this->getParameter('shape_dir'), $shape->getImage())) {
                $manager->remove($shape);
                $manager->flush();
                $this->addFlash('success',"Forme supprimée");
                return $this->redirectToRoute('shape_list');
            } else {
                $this->addFlash('error',"Image non trouvée");
                return $this->redirectToRoute('shape_list');
            }
        } else {
            $this->addFlash('error',"Forme non trouvée");
            return $this->redirectToRoute('shape_list');
        }
    }
}