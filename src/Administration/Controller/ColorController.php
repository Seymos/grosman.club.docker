<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 17:02
 */

namespace App\Administration\Controller;


use App\Administration\Form\ColorType;
use App\Shop\Entity\Color;
use App\Shop\Repository\ColorRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ColorController
 * @package App\Administration\Controller
 * @Route(path="/admin/colors")
 */
class ColorController extends AbstractController
{
    private $repository;

    public function __construct(ColorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(path="/", name="color_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request, EntityManagerInterface $manager): Response
    {
        $color = new Color();
        $form = $this->createForm(ColorType::class, $color);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $color->setSlug($slugger->slugify($color->getName()));
            $manager->persist($color);
            $manager->flush();
            $this->addFlash('success',"Couleur ajoutée !");
            return $this->redirectToRoute('color_list');
        }
        return $this->render(
            'administration/colors/list.html.twig',
            [
                'colors' => $this->repository->findAll(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/edit", name="color_edit")
     * @ParamConverter("color", class="App\Shop\Entity\Color")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, EntityManagerInterface $manager, Color $color): Response
    {
        $form = $this->createForm(ColorType::class, $color);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new Slugify();
            $color->setSlug($slugger->slugify($color->getName()));
            $manager->persist($color);
            $manager->flush();
            $this->addFlash('success',"Couleur modifiée !");
            return $this->redirectToRoute('color_list');
        }
        return $this->render(
            'administration/colors/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/{id}/remove", name="color_remove")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Color $color
     * @return Response
     */
    public function remove(Request $request, EntityManagerInterface $manager, Color $color): Response
    {
        if ($color) {
            $manager->remove($color);
            $manager->flush();
            $this->addFlash('success',"Couleur supprimée avec succès !");
            return $this->redirectToRoute('color_list');
        } else {
            $this->addFlash('success',"Couleur non trouvée !");
            return $this->redirectToRoute('color_list');
        }
    }
}