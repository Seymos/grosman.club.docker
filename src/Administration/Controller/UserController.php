<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 27/02/19
 * Time: 17:13
 */

namespace App\Administration\Controller;


use App\Administration\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Administration\Controller
 * @Route(path="/admin/customers")
 */
class UserController extends AbstractController
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(path="/", name="customer_list")
     * @return Response
     */
    public function list(): Response
    {
        return $this->render(
            'administration/customers/list.html.twig',
            [
                'customers' => $this->repository->findAll()
            ]
        );
    }
}
