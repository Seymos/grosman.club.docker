<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 18:04
 */

namespace App\Administration\Services;


use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    public function upload(UploadedFile $file, $targetDir, $filename = null): void
    {
        if ($filename === null) {
            $filename = md5(uniqid()).'.'.$file->guessExtension();
        } else {
            $filename = $filename.'.'.$file->guessExtension();
        }
        $file->move($targetDir, $filename);
    }

    public function remove($targetDir, $filename): bool
    {
        if (is_dir($targetDir)) {
            $handleDir = scandir($targetDir, SCANDIR_SORT_DESCENDING);
            dump($handleDir);
            dump($filename);
            foreach ($handleDir as $item) {
                if ($item === $filename) {
                    dump($item);
                    unlink($item);
                    return true;
                }
            }
        } else {
            return false;
        }
    }
}
