<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 26/02/19
 * Time: 11:12
 */

namespace App\Administration\Form;


use App\Shop\Entity\Patch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class
            )
            ->add(
                'image',
                FileType::class
            )
            ->add(
                'pricing',
                MoneyType::class
            )
            ->add(
                'quantity',
                IntegerType::class
            )
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Patch::class
        ]);
    }
}