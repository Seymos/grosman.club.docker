<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 17:52
 */

namespace App\Administration\Form;


use App\Shop\Entity\Shape;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShapeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class
            )
            ->add(
                'image',
                FileType::class
            )
            ->add(
                'pricing',
                MoneyType::class
            )
            ->add(
                'quantity',
                IntegerType::class
            )
            /*->addEventListener(
                FormEvents::PRE_SUBMIT,
                [$this, 'preSubmit']
            )*/
            ;
    }

    function preSubmit(FormEvent $event)
    {
        dump($event->getForm());
        die;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Shape::class
        ]);
    }
}
