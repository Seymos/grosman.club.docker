<?php

namespace App\Administration\Entity;

use App\Shop\Entity\Invoice;
use App\Shop\Entity\Ordering;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Administration\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var $plainPassword
     */
    protected $plainPassword;

    /**
     * @var $gender
     * @ORM\Column(type="string")
     */
    protected $gender;

    /**
     * @var $last_name
     * @ORM\Column(type="string")
     */
    protected $last_name;

    /**
     * @var $first_name
     * @ORM\Column(type="string")
     */
    protected $first_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Shop\Entity\Ordering", mappedBy="user", cascade={"persist"})
     */
    protected $orderings;

    /**
     * @ORM\ManyToMany(targetEntity="App\Administration\Entity\UserAddress", cascade={"persist"})
     * @ORM\JoinTable(name="users_addresses",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_address_id", referencedColumnName="id")})
     */
    private $addresses;

    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
        $this->addresses = new ArrayCollection();
        $this->orderings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function addRole(string $role)
    {
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
            return true;
        } else {
            return false;
        }
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name): void
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return Collection
     */
    public function getOrderings(): Collection
    {
        return $this->orderings;
    }

    /**
     * @param Ordering $ordering
     * @return void
     */
    public function addOrdering(Ordering $ordering): void
    {
        if (!$this->orderings->contains($ordering)) {
            $this->orderings->add($ordering);
            $ordering->setUser($this);
        }
    }

    /**
     * @return Collection
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    /**
     * @param UserAddress $address
     * @return User|boolean
     */
    public function addAddress(UserAddress $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses->add($address);
            return $this;
        } else {
            return false;
        }
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
            $this->roles,
            $this->gender
        ]);
    }

    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->roles,
            $this->gender
            ) = unserialize($serialized, [__CLASS__]);
    }

    public function __toString(): string
    {
        return $this->email;
    }
}
