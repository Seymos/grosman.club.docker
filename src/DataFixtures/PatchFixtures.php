<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 27/02/19
 * Time: 12:04
 */

namespace App\DataFixtures;


use App\Shop\Entity\Patch;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PatchFixtures extends Fixture
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 3; $i++) {
            $patch = new Patch();
            $patch->setName("Blason $i");
            $patch->setSlug("blason-$i");
            $patch->setQuantity(random_int(10,20));
            $patch->setPricing((float)random_int(10,20));
            $patch->setIsFixture(true);
            @chmod($this->container->getParameter('patch_dir'), 0755 & ~umask());
            copy($patch->getFixturesPath()."$i.jpg", $patch->getFixturesPath()."$i-copy.jpg");
            $file = new UploadedFile($patch->getFixturesPath()."$i-copy.jpg",$patch->getSlug(), null, null, null, true);
            $patch->setImage($file);
            $patch->setPath($patch->getFixturesPath());
            $manager->persist($patch);
            $manager->flush();
        }
    }
}
