<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 15:16
 */

namespace App\DataFixtures;


use App\Shop\Entity\Shape;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ShapeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 5; $i++) {
            $shape = new Shape();
            $shape->setName("Forme $i");
            $shape->setSlug("forme-$i");
            $shape->setQuantity(random_int(10,20));
            $shape->setIsFixture(true);
            $shape->setPricing((float)random_int(10,20));
            $shape->setPath($shape->getFixturesPath());
            //@chmod($this->container->getParameter('shape_dir'), 0755 & ~umask());
            copy($shape->getFixturesPath()."$i.jpg", $shape->getFixturesPath()."$i-copy.jpg");
            $file = new UploadedFile($shape->getFixturesPath()."$i-copy.jpg",$shape->getSlug(), null, null, null, true);
            $shape->setImage($file);
            $manager->persist($shape);
            $manager->flush();
        }
    }
}
