<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 24/02/19
 * Time: 18:51
 */

namespace App\DataFixtures;


use App\Administration\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setGender('mr');
        $user->setLastName('test');
        $user->setFirstName('test');
        $user->setEmail('test@test.fr');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'test'));
        $manager->persist($user);

        $user1 = new User();
        $user1->setGender('mr');
        $user1->setLastName('azer');
        $user1->setFirstName('azer');
        $user1->setEmail('azer@azer.fr');
        $user1->setPassword($this->passwordEncoder->encodePassword($user1, 'azer'));
        $manager->persist($user1);

        $user2 = new User();
        $user2->setGender('mr');
        $user2->setLastName('admin');
        $user2->setFirstName('admin');
        $user2->setEmail('admin@admin.fr');
        $user2->addRole('ROLE_ADMIN');
        $user2->setPassword($this->passwordEncoder->encodePassword($user2, 'admin'));
        $manager->persist($user2);

        $manager->flush();
    }
}
