<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 15:16
 */

namespace App\DataFixtures;


use App\Shop\Entity\Size;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SizeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $size = new Size();
        $size->setName("XS");
        $size->setSlug("xs");
        $size->setPricing((float)random_int(1,5));
        $size->setQuantity((float)random_int(5,10));
        $manager->persist($size);

        $size1 = new Size();
        $size1->setName("S");
        $size1->setSlug("s");
        $size1->setPricing((float)random_int(1,5));
        $size1->setQuantity((float)random_int(5,10));
        $manager->persist($size1);

        $size2 = new Size();
        $size2->setName("M");
        $size2->setSlug("m");
        $size2->setPricing((float)random_int(1,5));
        $size2->setQuantity((float)random_int(5,10));
        $manager->persist($size2);

        $size3 = new Size();
        $size3->setName("L");
        $size3->setSlug("l");
        $size3->setPricing((float)random_int(1,5));
        $size3->setQuantity((float)random_int(5,10));
        $manager->persist($size3);

        $size4 = new Size();
        $size4->setName("XL");
        $size4->setSlug("xl");
        $size4->setPricing((float)random_int(1,5));
        $size4->setQuantity((float)random_int(5,10));
        $manager->persist($size4);

        $manager->flush();

    }
}