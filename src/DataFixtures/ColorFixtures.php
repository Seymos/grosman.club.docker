<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 15:15
 */

namespace App\DataFixtures;


use App\Shop\Entity\Color;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ColorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $color = new Color();
        $color->setSlug('bleu');
        $color->setName('Bleu');
        $color->setCode('#0080C5');
        $color->setPricing(5);
        $color->setQuantity(5);
        $manager->persist($color);

        $color1 = new Color();
        $color1->setSlug('jaune');
        $color1->setName('Jaune');
        $color1->setCode('#FFF328');
        $color1->setPricing(5);
        $color1->setQuantity(5);
        $manager->persist($color1);

        $color2 = new Color();
        $color2->setSlug('rouge');
        $color2->setName('Rouge');
        $color2->setCode('#F8001E');
        $color2->setPricing(5);
        $color2->setQuantity(5);
        $manager->persist($color2);

        $color3 = new Color();
        $color3->setSlug('vert');
        $color3->setName('Vert');
        $color3->setCode('#00B465');
        $color3->setPricing(5);
        $color3->setQuantity(5);
        $manager->persist($color3);

        $color4 = new Color();
        $color4->setSlug('orange');
        $color4->setName('Orange');
        $color4->setCode('#FF9222');
        $color4->setPricing(5);
        $color4->setQuantity(5);
        $manager->persist($color4);

        $manager->flush();
    }
}