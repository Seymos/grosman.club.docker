<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 25/02/19
 * Time: 12:39
 */

namespace App\EventListener;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use App\Administration\Repository\UserRepository;

class TwigDataListener implements ServiceSubscriberInterface
{
    private $container;

    private $tokenStorage;

    public function __construct(ContainerInterface $container, TokenStorageInterface $tokenStorage)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest(GetResponseEvent $event): void
    {
        $request = $event->getRequest();
        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType() && $request instanceof Request) {
            $twig = $this->container->get('twig');
            $token = $this->tokenStorage->getToken();
            $user = $token->getUser()->getEmail();
            $db_user = $this->container->get(UserRepository::class)->findOneBy(['email' => $user->getEmail()]);
            dump($db_user);
            if ($token->isAuthenticated()) {
                $twig->addGlobal('current_token', $db_user);
            }
        }
    }

    public static function getSubscribedServices()
    {
        return ['onKernelRequest'];
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }
}
