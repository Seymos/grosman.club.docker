<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 27/02/19
 * Time: 11:14
 */

namespace App\Account\Form;


use App\Administration\Entity\UserAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAddressType extends ShippingType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'type',
                ChoiceType::class,
                array(
                    'choices' => array(
                        'Adresse de livraison' => 'shipping',
                        'Adresse de facturation' => 'billing'
                    )
                )
            )
            ->add(
                'number',
                TextType::class
            )
            ->add(
                'street',
                TextType::class
            )
            ->add(
                'zipCode',
                TextType::class
            )
            ->add(
                'city',
                TextType::class
            )
            ->add(
                'country',
                TextType::class
            )
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserAddress::class
        ]);
    }
}
