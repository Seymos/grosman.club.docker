<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 28/02/19
 * Time: 10:38
 */

namespace App\Account\Form;


use App\Administration\Entity\UserAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShippingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'number',
                TextType::class
            )
            ->add(
                'street',
                TextType::class
            )
            ->add(
                'zipCode',
                TextType::class
            )
            ->add(
                'city',
                TextType::class
            )
            ->add(
                'country',
                TextType::class
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserAddress::class
        ]);
    }
}