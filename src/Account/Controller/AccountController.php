<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 21/02/19
 * Time: 12:36
 */

namespace App\Account\Controller;

use App\Account\Form\UserAddressType;
use App\Administration\Entity\User;
use App\Administration\Entity\UserAddress;
use App\Shop\Entity\Ordering;
use App\Shop\Services\OrderingsService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountController
 * @package App\Account\Controller
 * @Route(path="/mon-compte")
 * @Security("is_granted('ROLE_USER')")
 */
class AccountController extends AbstractController
{
    /**
     * @Route(path="/", name="account_profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profile(): Response
    {
        return $this->render(
            'account/profile.html.twig'
        );
    }

    /**
     * @Route(path="/mes-commandes", name="account_orderings")
     * @return Response
     */
    public function orderings(OrderingsService $orderingsService): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        return $this->render(
            'account/orderings.html.twig',
            [
                'orderings' => $orderingsService->getLastOrdering($user)
            ]
        );
    }

    /**
     * @Route(path="/mes-commandes/{id}", name="account_ordering")
     * @ParamConverter("ordering", class="App\Shop\Entity\Ordering")
     * @param Ordering $ordering
     * @return Response
     */
    public function ordering(Ordering $ordering): Response
    {
        return $this->render(
            'account/ordering.html.twig',
            [
                'o' => $ordering
            ]
        );
    }

    /**
     * @Route(path="/mes-adresses", name="account_addresses")
     * @return Response
     */
    public function addresses(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $address = new UserAddress();
        $form = $this->createForm(UserAddressType::class, $address);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->addAddress($address);
            dump($address);
            die('create address');
        }
        return $this->render(
            'account/addresses.html.twig',
            [
                'addresses' => $user->getAddresses(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(path="/add-role/{role}", name="add_role")
     * @param string $role
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addRole(string $role, EntityManagerInterface $manager)
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->addRole($role)) {
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', "Vous détenez maintenant le rôle $role");
            return $this->redirectToRoute('account_profile');
        } else {
            $this->addFlash('error', "Vous détenez déjà le rôle $role");
            return $this->redirectToRoute('account_profile');
        }
    }
}
