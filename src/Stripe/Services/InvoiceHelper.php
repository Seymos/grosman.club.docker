<?php


namespace App\Stripe\Services;


use App\Shop\Entity\Invoice;
use App\Shop\Entity\Ordering;

class InvoiceHelper
{
    public static function generateInvoiceFor(Ordering $ordering, \DateTime $date_paid)
    {
        $invoice = new Invoice();
        $invoice->setOrdering($ordering);
        $invoice->setPaied(true);
        $invoice->setDatePaied($date_paid);
        $invoice->setReference($ordering->getReference());
        $invoice->setStatus('paid');
        $invoice->setTotalHt($ordering->getTotalHt());
        $invoice->setTotalVat($ordering->getTotalVat());
        $invoice->setTotalTtc($ordering->getTotalTtc());
        dump($invoice);
        exit('invoice generation');
        return $invoice;
    }
}
