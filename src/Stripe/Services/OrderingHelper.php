<?php


namespace App\Stripe\Services;


use App\Shop\Entity\Ordering;
use Doctrine\ORM\EntityManagerInterface;

class OrderingHelper
{
    public static function changeOrderingStatus(Ordering $ordering, string $status)
    {
        if ('waiting' === $ordering->getStatus()) {
            $ordering->setStatus($status);
        }
        return $ordering;
    }
}
