<?php


namespace App\Stripe\Services;


use App\Administration\Entity\User;
use App\Shop\Entity\Invoice;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailHelper
{
    private $mailer;
    private $container;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->container = $container;
    }

    public function emailCustomer(array $emailParams, User $user, Invoice $invoice)
    {
        // send swift message with attached invoice
//        $this->container->get('templating')->render();
        // return true if message is sent
    }
}
