<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 02/03/19
 * Time: 21:29
 */

namespace App\Stripe\Controller;


use App\Administration\Entity\User;
use App\Shop\Entity\Invoice;
use App\Shop\Entity\Ordering;
use App\Stripe\Services\EmailHelper;
use App\Stripe\Services\InvoiceHelper;
use App\Stripe\Services\OrderingHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PaymentsController
 * @package App\Stripe\Controller
 * @Route(path="/stripe/payments/")
 */
class PaymentsController extends AbstractController
{
    /**
     * @Route(path="checkout2/", name="stripe_payments_checkout")
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $manager
     * @throws \Exception
     */
    public function checkout2(Request $request, SessionInterface $session, EntityManagerInterface $manager)
    {
        if ($request->isMethod('POST')) {
            $api_key = "sk_test_MYktON6LC0y7zYmqdEu63ZjN";
            \Stripe\Stripe::setApiKey($api_key);
            $charge = \Stripe\Charge::create([
                'amount' => $request->request->get('amount')*100,
                'currency' => $request->request->get('currency'),
                'source' => $request->request->get('stripe_token'),
            ]);
            dump($charge->paid);
            if (true === $charge->paid) {
                $date_paid = new \DateTime('now');
                /** @var Ordering $ordering */
                $ordering = $session->get('ordering');
                /** @var User $user */
                $user = $this->getUser();
                // change ordering status to paied
                $ordering = OrderingHelper::changeOrderingStatus($ordering, 'paid');
                dump($ordering);
                // generate invoice + save
                $invoice = InvoiceHelper::generateInvoiceFor($ordering, $date_paid);
                dump($invoice);
                exit('checkout 2');
//                $manager->flush();
                // send email to customer
//                $emailParams = [];
//                $emailHelper->emailCustomer($emailParams, $user, $invoice);
                // redirect to account -> order + invoice
            }
            exit('POST METHOD');
        } else {
            dump($request->getMethod());
            exit('NOT POST METHOD');
        }
    }

    /**
     * @Route(path="/checkout", name="stripe_payments_checkout_to_replace")
     * @param Request $request
     * @param SessionInterface $session
     * @param InvoiceHelper $invoiceHelper
     * @param EmailHelper $emailHelper
     * @return Response
     */
    public function checkout(Request $request, SessionInterface $session, InvoiceHelper $invoiceHelper, EmailHelper $emailHelper): Response
    {
        if ($request->isMethod('POST')) {
            $api_key = "sk_test_MYktON6LC0y7zYmqdEu63ZjN";
            \Stripe\Stripe::setApiKey($api_key);
            $charge = \Stripe\Charge::create([
                'amount' => $request->request->get('amount')*100,
                'currency' => $request->request->get('currency'),
                'source' => $request->request->get('stripe_token'),
            ]);
            dump($charge->paid);
            if (true === $charge->paid) {
                /** @var Ordering $ordering */
                $ordering = $session->get('ordering');
                /** @var User $user */
                $user = $this->getUser();
                // change ordering status to paied
                $changed = OrderingHelper::changeOrderingStatus($ordering, 'paid');
                dump($changed);
                // generate invoice + save
                $invoice = self::generateInvoiceFor($ordering);
                // send email to customer
                $emailParams = [];
                $emailHelper->emailCustomer($emailParams, $user, $invoice);
                // redirect to account -> order + invoice
            }
            die('stripe charge');
        } else {
            exit('not post');
        }
    }

    private static function generateInvoiceFor(Ordering $ordering)
    {
        $invoice = new Invoice();
        $invoice->setOrdering($ordering);
        $invoice->setPaied(true);
        $invoice->setReference();
        dump($invoice, $ordering);
        exit('invoice generation');
        return $invoice;
    }
}
